import numpy, scipy.sparse, scipy.linalg

def computePrecon( blockList, JacobiFuncs, stable, systemMatrix ):
  precon = scipy.sparse.lil_matrix( systemMatrix.shape ) # initate empty matrix
  for block in blockList: # loop over blocks
    if numpy.any(stable[block]): # if block has stable functions
      stableBlock = block[stable[block]]
      blockInv = scipy.linalg.inv( systemMatrix[stableBlock,:][:,stableBlock].todense() ) # compute local inverse
      precon[numpy.ix_(stableBlock,stableBlock)] += blockInv # add to matrix
    if len(JacobiFuncs)>0:
      precon[JacobiFuncs,JacobiFuncs] = 1/systemMatrix[JacobiFuncs,JacobiFuncs] # Jacobi for not trimmed functions
  return scipy.sparse.csr_matrix(precon)

# loop over the blocks to determine unstable basis functions
def eliminate( systemMatrix, blockList, thresHold ):
  stable = systemMatrix.diagonal()>=thresHold # boolean array indicating stabilit of function
  for block in blockList: # loop over blocks
    while numpy.any(stable[block]): # if the block has at least one stable function
      stableBlock = block[stable[block]] # restrict block to the stable functions
      localVals,localVecs = numpy.linalg.eig(systemMatrix[stableBlock,:][:,stableBlock].todense()) # eigenvalues of (stable) block-matrix
      smallestVal = numpy.amin(abs(localVals)) # smallest eigenvalue of block
      if smallestVal < thresHold: # block is not stable: eliminate a function and test stability again
        localSmallestMode = numpy.argmin(abs(localVals)) # local index of smallest mode
        localSmallestVec = numpy.ravel(localVecs[:,localSmallestMode]) # local smallest eigenvector
        localUnstableIndex = numpy.argmax(abs(localSmallestVec)) # local index of most unstable (dominant) basis function
        globalUnstableIndex = stableBlock[localUnstableIndex] # global index
        stable[globalUnstableIndex] = False # set array index to unstable
      else: # block is stable: continue to next block
        break
  return stable

# sort functions in blocks and Jacobi functions
def sortFuncs( supportedElems, fullElems, yNElems, xNElems, degree, vector ):
  yNFuncs = yNElems+degree # number of y functions
  xNFuncs = xNElems+degree # number of x functions

  blockList  = [] # initiate block list
  cutFuncs = set() # initiate set of trimmed functions (that are supported on a trimmed element)
 
  for elem in supportedElems.difference(fullElems): # loop over cut elements
    xElem = numpy.floor_divide( elem, yNElems ) # x index of the element
    yElem = numpy.remainder( elem, yNElems ) # y index of the element

    yFuncs = yElem + numpy.reshape( numpy.arange( degree+1 ), (-1, 1) ) # y indices of supported functions
    xFuncs = xElem + numpy.reshape( numpy.arange( degree+1 ), (1, -1) ) # x indices of supported functions

    block = numpy.ravel( yFuncs + yNFuncs*xFuncs ) # global indexing of functions in block
    blockList.append( block ) # add block to list
    if vector: # for vector functions also devise a block for the functions in the x direction
      blockList.append( block+yNFuncs*xNFuncs )
    cutFuncs.update( block ) # add global indices of cut functions to the list

  fullElemFuncs = set() # initiate set of functions that are supported on (at least one) full (not trimmed) element
 
  for elem in fullElems: # loop over full elements
    xElem = numpy.floor_divide( elem, yNElems )
    yElem = numpy.remainder( elem, yNElems )

    yFuncs = yElem + numpy.reshape( numpy.arange( degree+1 ), (-1, 1) )
    xFuncs = xElem + numpy.reshape( numpy.arange( degree+1 ), (1, -1) )

    fullElemFuncs.update( numpy.ravel( yFuncs + yNFuncs*xFuncs ) )

  JacobiFuncs = numpy.array(list(fullElemFuncs.difference(cutFuncs))) # functions that are only supported on full elements
  if vector: # for vector functions also add the function in the x direction
    JacobiFuncs = numpy.concatenate(( JacobiFuncs, JacobiFuncs+yNFuncs*xNFuncs ))

  return blockList, JacobiFuncs

# same as sortFuncs but for the Taylor-Hood basis
def sortFuncsTH( supportedElems, fullElems, yNElems, xNElems, degree, vector ):
  yNFuncs = 2*yNElems+degree-1
  xNFuncs = 2*xNElems+degree-1

  blockList  = []
  cutFuncs = set()
 
  for elem in supportedElems.difference(fullElems):
    xElem = numpy.floor_divide( elem, yNElems )
    yElem = numpy.remainder( elem, yNElems )

    yFuncs = 2*yElem + numpy.reshape( numpy.arange( degree+1 ), (-1, 1) )
    xFuncs = 2*xElem + numpy.reshape( numpy.arange( degree+1 ), (1, -1) )

    block = numpy.ravel( yFuncs + yNFuncs*xFuncs )
    blockList.append( block )
    if vector:
      blockList.append( block+yNFuncs*xNFuncs )
    cutFuncs.update( block )

  fullElemFuncs = set()
 
  for elem in fullElems:
    xElem = numpy.floor_divide( elem, yNElems )
    yElem = numpy.remainder( elem, yNElems )

    yFuncs = 2*yElem + numpy.reshape( numpy.arange( degree+1 ), (-1, 1) )
    xFuncs = 2*xElem + numpy.reshape( numpy.arange( degree+1 ), (1, -1) )

    fullElemFuncs.update( numpy.ravel( yFuncs + yNFuncs*xFuncs ) )

  JacobiFuncs = numpy.array(list(fullElemFuncs.difference(cutFuncs)))
  if vector:
    JacobiFuncs = numpy.concatenate(( JacobiFuncs, JacobiFuncs+yNFuncs*xNFuncs ))

  return blockList, JacobiFuncs

# sort functions in blocks and Jacobi functions
def sortFuncs3d( supportedElems, fullElems, zNElems, yNElems, xNElems, degree, vector ):
  zNFuncs = zNElems+degree # number of z functions
  yNFuncs = yNElems+degree # number of y functions
  xNFuncs = xNElems+degree # number of x functions

  blockList  = [] # initiate block list
  cutFuncs = set() # initiate set of trimmed functions (that are supported on a trimmed element)
 
  for elem in supportedElems.difference(fullElems): # loop over cut elements
    xElem = numpy.floor_divide( elem, zNElems*yNElems ) # x index of the element
    yElem = numpy.floor_divide( numpy.remainder( elem , zNElems*yNElems ), zNElems ) # y index of the element
    zElem = numpy.remainder( elem, zNElems ) # z index of the element

    zFuncs = zElem + numpy.reshape( numpy.arange( degree+1 ), (-1, 1, 1) ) # z indices of supported functions
    yFuncs = yElem + numpy.reshape( numpy.arange( degree+1 ), (1, -1, 1) ) # y indices of supported functions
    xFuncs = xElem + numpy.reshape( numpy.arange( degree+1 ), (1, 1, -1) ) # x indices of supported functions

    block = numpy.ravel( zFuncs + zNFuncs*yFuncs + zNFuncs*yNFuncs*xFuncs ) # global indexing of functions in block
    blockList.append( block ) # add block to list
    if vector: # for vector functions also devise a block for the functions in the y and x direction
      blockList.append( block+zNFuncs*yNFuncs*xNFuncs ) # y direction
      blockList.append( block+2*zNFuncs*yNFuncs*xNFuncs ) # x direction
    cutFuncs.update( block ) # add global indices of cut functions to the list

  fullElemFuncs = set() # initiate set of functions that are supported on (at least one) full (not trimmed) element
 
  for elem in fullElems: # loop over full elements
    xElem = numpy.floor_divide( elem, zNElems*yNElems )
    yElem = numpy.floor_divide( numpy.remainder( elem , zNElems*yNElems ), zNElems )
    zElem = numpy.remainder( elem, zNElems )

    zFuncs = zElem + numpy.reshape( numpy.arange( degree+1 ), (-1, 1, 1) )
    yFuncs = yElem + numpy.reshape( numpy.arange( degree+1 ), (1, -1, 1) )
    xFuncs = xElem + numpy.reshape( numpy.arange( degree+1 ), (1, 1, -1) )

    fullElemFuncs.update( numpy.ravel( zFuncs + zNFuncs*yFuncs + zNFuncs*yNFuncs*xFuncs ) )

  JacobiFuncs = numpy.array(list(fullElemFuncs.difference(cutFuncs))) # functions that are only supported on full elements
  if vector: # for vector functions also add the functions in the y and x directions
    JacobiFuncs = numpy.concatenate(( JacobiFuncs, JacobiFuncs+zNFuncs*yNFuncs*xNFuncs, JacobiFuncs+2*zNFuncs*yNFuncs*xNFuncs ))

  return blockList, JacobiFuncs

# same as sortFuncs3d but for the Taylor-Hood basis
def sortFuncs3dTH( supportedElems, fullElems, zNElems, yNElems, xNElems, degree, vector ):
  zNFuncs = 2*zNElems+degree-1
  yNFuncs = 2*yNElems+degree-1
  xNFuncs = 2*xNElems+degree-1

  blockList  = []
  cutFuncs = set()
 
  for elem in supportedElems.difference(fullElems):
    xElem = numpy.floor_divide( elem, zNElems*yNElems )
    yElem = numpy.floor_divide( numpy.remainder( elem , zNElems*yNElems ), zNElems )
    zElem = numpy.remainder( elem, zNElems )

    zFuncs = 2*zElem + numpy.reshape( numpy.arange( degree+1 ), (-1, 1, 1) )
    yFuncs = 2*yElem + numpy.reshape( numpy.arange( degree+1 ), (1, -1, 1) )
    xFuncs = 2*xElem + numpy.reshape( numpy.arange( degree+1 ), (1, 1, -1) )

    block = numpy.ravel( zFuncs + zNFuncs*yFuncs + zNFuncs*yNFuncs*xFuncs )
    blockList.append( block )
    if vector:
      blockList.append( block+zNFuncs*yNFuncs*xNFuncs )
      blockList.append( block+2*zNFuncs*yNFuncs*xNFuncs )
    cutFuncs.update( block )

  fullElemFuncs = set()
 
  for elem in fullElems:
    xElem = numpy.floor_divide( elem, zNElems*yNElems )
    yElem = numpy.floor_divide( numpy.remainder( elem , zNElems*yNElems ), zNElems )
    zElem = numpy.remainder( elem, zNElems )

    zFuncs = 2*zElem + numpy.reshape( numpy.arange( degree+1 ), (-1, 1, 1) )
    yFuncs = 2*yElem + numpy.reshape( numpy.arange( degree+1 ), (1, -1, 1) )
    xFuncs = 2*xElem + numpy.reshape( numpy.arange( degree+1 ), (1, 1, -1) )

    fullElemFuncs.update( numpy.ravel( zFuncs + zNFuncs*yFuncs + zNFuncs*yNFuncs*xFuncs ) )

  JacobiFuncs = numpy.array(list(fullElemFuncs.difference(cutFuncs)))
  if vector:
    JacobiFuncs = numpy.concatenate(( JacobiFuncs, JacobiFuncs+zNFuncs*yNFuncs*xNFuncs, JacobiFuncs+2*zNFuncs*yNFuncs*xNFuncs ))

  return blockList, JacobiFuncs
