import numpy, scipy.sparse.linalg

def preconCG( systemMatrix, preconMatrix, rhs, maxiter, tolerance ):
 
  x = numpy.zeros(rhs.shape)
  r = rhs
  z = preconMatrix.dot(r)
  rho = r.dot(z) 
  p = z 

  residual = [numpy.linalg.norm(r,2)]

  for iteration in numpy.arange( int(maxiter) ):

    q = systemMatrix.dot(p)
    alpha = rho/p.dot(q) 
    x = x + alpha*p 
    r -= alpha*q
    z = preconMatrix.dot(r)
    rho_old = rho 
    rho = r.dot(z) 
    beta = rho/rho_old 
    p = z + beta*p 
    if numpy.logical_not(numpy.isfinite(rho)) or (rho<=0):
      breakdown = True
      break 

    residual.append( numpy.linalg.norm(r,2) )
    print('relative residual', iteration+1, '=', residual[-1]/residual[0])

    if residual[-1]/residual[0] < tolerance:
      break

  return x, residual

def power( A, maxiter=1e2 ):

  v = numpy.ones(A.shape[0])
  for niter in numpy.arange(int(maxiter)):
    v = A.dot(v)
    v = v/numpy.linalg.norm(v,2)

  maxEigenvalue = numpy.linalg.norm(A.dot(v),2)/numpy.linalg.norm(v,2)

  v = numpy.ones(A.shape[0])
  for niter in numpy.arange(int(maxiter)):
    v = scipy.sparse.linalg.spsolve( A, v )
    v = v/numpy.linalg.norm(v,2)

  minEigenvalue = numpy.linalg.norm(A.dot(v),2)/numpy.linalg.norm(v,2)

  return maxEigenvalue/minEigenvalue
