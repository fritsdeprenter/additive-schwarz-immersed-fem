#!/usr/bin/env python3

# import libraries
import numpy, scipy.sparse, scipy.sparse.linalg, matplotlib.pyplot, pickle, preconditioners, solvers

# object to store residual in GMRES solver
class gmres_residual_storing(object):
  def __init__(self,initial_residual):
    self.residual = [initial_residual]
    self.counter = 0
    print('residual', self.counter, ' = ', self.residual[-1]/self.residual[0])
  def __call__(self,residual):
    self.residual.append( residual )
    self.counter += 1
    print('residual', self.counter, ' = ', self.residual[-1]/self.residual[0])

# definition to load external files
def load_dataFile(path):
  with open(path, 'rb') as fileName:
    return pickle.load(fileName)

# input
PDE = 'NavierStokes'
nElems = 8
degree = 2
relativeThreshold = 1e-14
relativeThreshold_Schur = 1e-11
maxiter = int(5e3)
tolerance = 2**-30
 
# load data
if PDE == 'Stokes':
  supportedElems, fullElems, systemMatrix_vu, systemMatrix_vp, systemMatrix_qp, rhs_vu, rhs_qp, SchurApproxMatrix = load_dataFile('stored/popcornStokes_nElems_'+str(nElems)+'_degree_'+str(degree)) # Stokes
elif PDE == 'NavierStokes':
  supportedElems, fullElems, systemMatrix_vu, systemMatrix_vp, systemMatrix_qp, rhs_vu, rhs_qp, SchurApproxMatrix = load_dataFile('stored/popcornNavier_nElems_'+str(nElems)+'_degree_'+str(degree)) # NavierStokes

# set blocks and untrimmed functions, the full ambient domain is (-1,1)^2 such that the total number of elements is multiplied by 2
blockList_vu, JacobiFuncs_vu = preconditioners.sortFuncs3dTH( supportedElems, fullElems, nElems, nElems, nElems, degree, True )
blockList_qp, JacobiFuncs_qp = preconditioners.sortFuncs3d( supportedElems, fullElems, nElems, nElems, nElems, degree-1, False )
# set stability threshold
thresHold_vu = relativeThreshold*numpy.amax(systemMatrix_vu.diagonal())
thresHold_qp = relativeThreshold_Schur*numpy.amax(SchurApproxMatrix.diagonal())
# unstable functions
stable_vu = preconditioners.eliminate( systemMatrix_vu, blockList_vu, thresHold_vu )
stable_qp = preconditioners.eliminate( SchurApproxMatrix, blockList_qp, thresHold_qp )
# construct preconditioner
precon_vu = preconditioners.computePrecon( blockList_vu, JacobiFuncs_vu, stable_vu, systemMatrix_vu )
precon_vp  = scipy.sparse.csr_matrix( systemMatrix_vp.shape )
precon_qp = preconditioners.computePrecon( blockList_qp, JacobiFuncs_qp, stable_qp, SchurApproxMatrix )

# assemble
systemMatrix = scipy.sparse.csr_matrix(scipy.sparse.vstack([ scipy.sparse.hstack([ systemMatrix_vu  , systemMatrix_vp ]),
                                                             scipy.sparse.hstack([ systemMatrix_vp.T, systemMatrix_qp ]) ]))
rhs = numpy.hstack([ rhs_vu, rhs_qp ])
precon = scipy.sparse.csr_matrix(scipy.sparse.vstack([ scipy.sparse.hstack([ precon_vu  , precon_vp ]),
                                                       scipy.sparse.hstack([ precon_vp.T, precon_qp ]) ]))

# solve
residualObject = gmres_residual_storing(numpy.linalg.norm(precon.dot(rhs))/numpy.linalg.norm(rhs)) 
# the initial preconditioned residual is scaled with the inital unpreconditioned residual (this is what scipy does as well) 
sol,info = scipy.sparse.linalg.gmres( systemMatrix, rhs, M=precon, tol=tolerance*numpy.linalg.norm(precon.dot(rhs))/numpy.linalg.norm(rhs), restart=maxiter, maxiter=maxiter, callback=residualObject )
# the relative tolerance is scaled with the preconditioned and unpreconditioned initial residual (this is for the tolerance of scipy to work correctly)

# output 
matplotlib.pyplot.loglog( numpy.arange(1,len(residualObject.residual)+1), numpy.array(residualObject.residual)/residualObject.residual[0])
matplotlib.pyplot.title('residual')
matplotlib.pyplot.show()
